import numpy as np
from sklearn import neighbors, datasets
from sklearn import preprocessing 
def main():
 
  n_neighbors = 6
 
  iris = datasets.load_iris()
  X = iris.data[:, :2]  
  y = iris.target
  h = .02 
 
  clf = neighbors.KNeighborsClassifier(n_neighbors, weights='distance')
  clf.fit(X, y)
  sl = 2.5
  sw = 3.6
  dataClass = clf.predict([[sl,sw]])
  print('Prediction: ')
 
  if dataClass == 0:
      print('Iris Setosa')
  elif dataClass == 1:
      print('Iris Versicolour')
  else:
      print('Iris Virginica')

if __name__=='__main__':
    main()
