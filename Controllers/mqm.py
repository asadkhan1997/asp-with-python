import sys



import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
import pandas as pd

def main():
    print('This is Regression\n')
   
        # print concatenated parameters
    df = pd.read_csv("ABC.csv")
    Y = df['price']
    X = df['lotsize']
    X=X.values.reshape(len(X),1)
    Y=Y.values.reshape(len(Y),1)
    X_train = X[:-250]
    X_test = X[-250:]
    Y_train = Y[:-250]
    Y_test = Y[-250:]
    regr = linear_model.LinearRegression()
 
    regr.fit(X_train, Y_train)
    print( str(regr.predict(5000)) )

if __name__=='__main__':
    main()